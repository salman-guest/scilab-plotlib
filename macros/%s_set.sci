function %s_set(h,varargin)
  
  if h<>0
    error('set : 0 (root window) is the only allowed scalar value');
  end
  
  for i=1:2:length(varargin)-1  
  
    prop=convstr(varargin(i));
  
    if strindex(prop,'default')<>1
      disp('set : only default property are a allowed for the root window');
      break;  
    end
    
    prop=strsubst(prop,'default','');
    
    if strindex(prop,'figure')==1
      prop=strsubst(prop,'figure','');
      p_handle=mlist(['pltlibH','handle'],gdf());  
    elseif strindex(prop,'axes')==1
      prop=strsubst(prop,'axes','');
      p_handle=mlist(['pltlibH','handle'],gda());  
    else
      disp('set : unknown/unsupported object default property')   ;
      break;
    end
    
    %pltlibH_set(p_handle,prop,varargin(i+1));
        
  end
   
endfunction
  
