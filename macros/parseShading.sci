function out=parseShading(typeOfPlot,value,pptystring,ppty)

if type(value)==10
   select value
   case 'mean'
      out=list("FaceColor","mean","EdgeColor","none");
   case 'flat'
      out=list("FaceColor","flat","EdgeColor","none");
   case 'faceted'
      out=list("FaceColor","flat","EdgeColor","default");
   case 'interp'
      out=list("FaceColor","interp","EdgeColor","none");
   else
     _error(sprintf('%s : unknown shading specification ''%s''',typeOfPlot,value))
   end
else
   _error(sprintf('%s : shading specification must be a string',typeOfPlot))
end

endfunction // end of parseShading
