function [ax,h1,h2]=_plotyy(x1,y1,x2,y2,varargin)
  
  if argn(2)==0
    x = 0:0.01:20;
    y1 = 200*exp(-0.05*x).*sin(x);
    y2 = 0.8*exp(-0.5*x).*sin(10*x);
    [ax,h1,h2] = _plotyy(x,y1,x,y2,'plot');
    ylabel(ax(1),'Left Y-Axis');
    ylabel(ax(2),'Right Y-Axis');
    xlabel(ax(3),'Common X-Axis');
    title(ax(3),'plotyy example');
    return
  end  
  i=1;
  while i<=min(length(varargin),2)
    if type(varargin(i))==10
      fun(i)=varargin(i);      
    else
      _error(sprintf('plotyy : argument %d has not the expected type',i))
    end
    i=i+1;
  end
  if i==1
    fun=['plot','plot'];
  elseif i==2
    fun(2)=fun(1);
  end
  
  win=_gcf();
  IMD=win.immediate_drawing;
  win.immediate_drawing='off';
  main=_gca();    
  col=get(main,'ColorOrder');
  pos=get(main,'Position');
  main=_subplot('Position',pos);
  _cla(main);
  ax1=_axes('Position',pos,'YAxisLocation','left','YColor',col(1,:));
  ax2=_axes('Position',pos,'YAxisLocation','right','YColor',col(2,:));
  h1=_mainPlot(fun(1),list(ax1,x1,y1,'color',col(1,:)));
  h2=_mainPlot(fun(2),list(ax2,x2,y2,'color',col(2,:)));
  xlim=[min(min(x1),min(x2)),max(max(x1),max(x2))];
  xsc=get(ax1,'xscale');
  %pltlibH_set(main,'XLim',xlim,'XScale',xsc,'YTick',[],'visible','on','box','on');
  %pltlibH_set(ax1,'XLim',xlim,'color','none','box','off','xcolor','none');
  %pltlibH_set(ax2,'XScale',xsc,'XLim',xlim,'color','none','box','off','xcolor','none');
  ax=[ax1;ax2;main];
  win.immediate_drawing=IMD;
endfunction
