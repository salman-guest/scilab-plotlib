function hdl=_plot(varargin)

[lhs,rhs]=argn(0);

if ~rhs
   drawlater;
   clf();
   t=linspace(0,2*%pi,64);
   _subplot(2,2,1); 
   h1=_plot(t,cos(t));
   title 'plot(t,cos(t))';
   _subplot(2,2,2); 
   h2=_plot(t,[cos(t);sin(t);sin(t).*cos(t)]);
   title 'plot(t,[cos(t);sin(t);sin(t).*cos(t)])';
   _subplot(2,2,3); 
   h3=_plot(t,cos(t),t,cos(t),'og');
   title 'plot(t,cos(t),t,cos(t),''og'''
   _subplot(2,2,4); 
   h4=_plot(cos(t),sin(t));
   title 'plot(cos(t),sin(t)'
   _axis equal
   hdl=[h1;h2;h3;h4];
   drawnow;
   return
end

hdl=_mainPlot('plot',varargin);

// end of plot
endfunction  

