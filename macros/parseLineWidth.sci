function out=parseLineWidth(typeOfPlot,value,pptystring,ppty)

  select type(value)
  case 1
    if length(value)==1
       out=list(ppty,min(9,max(1,round(value))));     
    end
  else
     _error(sprintf('%s : missing LineWidth spec',typeOfPlot));
  end


endfunction
