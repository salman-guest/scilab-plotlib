function out=parseAxis(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'axis' property 
//

out=list();

select type(value)
case 10 //  a string
   if value=="tight"
//      out=list('XLimMode',"manual",'YLimMode',"manual",'ZLimMode',"manual");
   elseif value=="left"
      out=list("XAxisLocation","bottom","YAxisLocation","left");
   elseif value=="right"
      out=list("XAxisLocation","bottom","YAxisLocation","right");
   elseif or(value==["center";"origin"])
      out=list("XAxisLocation","middle","YAxisLocation","middle","ZAxisLocation","middle");
   elseif value=="box"
      out=list('XTick',[],'YTick',[],'ZTick',[]);
   elseif or(value==["on","off"])
      out=list("Visible",value);
   elseif value=="equal"
      out=list("DataAspectRatio",[1 1 1]);
   elseif value=="vis3d"
      out=list("DataAspectRatio",[1 1 1]);
   elseif value=="normal"
      out=list("DataAspectRatioMode","auto");
   elseif value=="auto"
      out=list("XLimMode","auto","YLimMode","auto","ZLimMode","auto");
   elseif value=="manual"
      out=list("XLimMode","manual","YLimMode","manual","ZLimMode","manual");
   elseif value=="ij"
      out=list("XDir","normal","YDir","reverse");
   elseif value=="xy"
      out=list("XDir","normal","YDir","normal");
   else
      _error(sprintf('%s : unknown axis mode ''%s''',typeOfPlot,value));
   end
case 1 // a matrix (must be a 4 or 6 element vector)
   if (length(value)==6) & or(size(value)==1)
      out=list("XLim",value(1:2),"YLim",value(3:4),"ZLim",value(5:6));
   elseif (length(value)==4) & or(size(value)==1)
      out=list("XLim",value(1:2),"YLim",value(3:4));
   else
      _error(sprintf('%s : axis limits must be a 4 or 6 element vector',typeOfPlot));
   end
else
   _error(sprintf('%s : missing axis limits or mode ',typeOfPlot));
end




endfunction
