function out=parseOnOff(typeOfPlot,value,pptystring,ppty)

select type(value)
case 10 //  a string
  if and(value~=["on","off"])
       _error(sprintf('%s : % value must be ""on"" or ""off""',typeOfPlot,ppty));
    end
else
   _error(sprintf('%s : missing %s spec',typeOfPlot,ppty));
end

out=list(ppty,value);

endfunction
