function %pltlibH_p(h) 
  if ~is_handle_valid(h.handle)
    printf('handle is no more valid\n');
  else
    if length(h.handle)==1
      printf('Plotlib graphic handle of %s type\n',h.handle.type); 
    else
      printf('%d x %d matrix of Plotlib graphic handles of type:\n',size(h.handle)); 
      for i=1:size(h.handle,1)
        printf('\n');
        for j=1:size(h.handle,2)
          printf('%-9s',h.handle(i,j).type); 
        end  
      end
    end
  end
endfunction
