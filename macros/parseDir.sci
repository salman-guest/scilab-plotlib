function out=parseDir(typeOfPlot,value,pptystring,ppty)

select type(value)
case 10 //  a string
  if and(value~=["normal","reverse"])
       _error(sprintf('%s : % value must be ""normal"" or ""reverse""',typeOfPlot,ppty));
    end
else
   _error(sprintf('%s : missing %s spec',typeOfPlot,ppty));
end

out=list(ppty,value);

endfunction
