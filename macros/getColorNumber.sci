function [_p_v_list,fail]=getColorNumber(typeOfPlot,ch) 

_p_v_list=list();

c=["";""];
m="";
LineStyle="";
fail=%f;
sizes=[0 0];

if length(ch)==0
  return
end

// Line type modifiers

if strindex(ch,'--')
   ch=strsubst(ch,'--','');
   LineStyle="--";
elseif strindex(ch,'-.')
   ch=strsubst(ch,'-.','');
   LineStyle="-."
elseif strindex(ch,'.-')
   ch=strsubst(ch,'.-','');
   LineStyle="-."
elseif strindex(ch,'-')
   ch=strsubst(ch,'-','');
   LineStyle="-";
elseif strindex(ch,':')
   ch=strsubst(ch,':','');
   LineStyle=":";
end   

for i=1:length(ch)
  _char=part(ch,i);
  if strindex("ymcbgrwk",_char)~=[] & or(c==["";""])
    if m==""
      c(1)=_char;
    else
      c(2)=_char;
    end
elseif strindex(".+x*tdfo<>^v",_char)~=[] & m==""
    m=_char;
  elseif strindex("123456789",_char)~=[] & or(sizes==[0 0])
    if m==""
      sizes(1)=evstr(_char);
    else
      sizes(2)=evstr(_char);
    end
  else
     fail=%t;
     return;
  end // if
end // for


if LineStyle == "" & m==""
//  _p_v_list=lstcat(_p_v_list,"LineStyle","-");
  if c(1)~=""
    _p_v_list=lstcat(_p_v_list,"Color",c(1));
 end
elseif m==""
  _p_v_list=lstcat(_p_v_list,"LineStyle",LineStyle);
  if c(1)~=""
    _p_v_list=lstcat(_p_v_list,"Color",c(1));
  end
elseif LineStyle==""
  if or(typeOfPlot==["quiver" "quiver3"])
    _p_v_list=lstcat(_p_v_list,"Marker",m);
  else
    _p_v_list=lstcat(_p_v_list,"Marker",m,"LineStyle","none");
  end
  if c(1)~=""
    _p_v_list=lstcat(_p_v_list,"MarkerFaceColor",c(1));
  end
  if c(2)~=""
    _p_v_list=lstcat(_p_v_list,"MarkerEdgeColor",c(2));
  end
else
  _p_v_list=lstcat(_p_v_list,"LineStyle",LineStyle,"Marker",m);
  if c(1)~="" & c(2)==""
    _p_v_list=lstcat(_p_v_list,"Color",c(1),"MarkerEdgeColor",c(1));
  elseif c(1)=="" & c(2)~=""
    _p_v_list=lstcat(_p_v_list,"Color",c(2),"MarkerEdgeColor",c(2));
  elseif c(1)~="" & c(2)~=""
    _p_v_list=lstcat(_p_v_list,"Color",c(1),"MarkerEdgeColor",c(2));
  end
end

if sizes(1)
   _p_v_list=lstcat(_p_v_list,"LineWidth",sizes(1));
end
if sizes(2)
   _p_v_list=lstcat(_p_v_list,"MarkerSize",sizes(2));
end


// end of getColorNumber
endfunction
