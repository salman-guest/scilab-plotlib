function [_axes_ppty_value_list,_next]=_parse_axes_ppty_value(typeOfPlot,argList,_start)
  
global axesPropertiesNames
    
_next=_start;
_axes_ppty_value_list=list();

while _next <= length(argList)

  if type(argList(_next))~=10
    break;
  end    
  
  if _next == length(argList)
      _error(sprintf('%s : unknown property name ""%s"" or missing value',typeOfPlot,argList(_next)));
  else    
      pptystring=convstr(argList(_next));
      value=argList(_next+1);

      if or(pptystring==axesPropertiesNames(1))
      
            ppty=axesPropertiesNames(pptystring)(1);              
            parseFunction=axesPropertiesNames(pptystring)(2);
            cmd=sprintf("%s(typeOfPlot,value,""%s"",""%s"")",parseFunction,pptystring,ppty);
            _ppty_value_list=evstr(cmd);
            _axes_ppty_value_list=lstcat(_axes_ppty_value_list,_ppty_value_list);
     
      else
           _error(sprintf('%s : ""%s"" is an unknown property name',typeOfPlot,pptystring));
      end // select convstr(_pair(1))

     _next=_next+2;

  end

end  
  
endfunction
