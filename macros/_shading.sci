function _shading(varargin)


i=1;
ax=get('current_axes');
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
       ax=varargin(i).handle;
     else
      _error("axis : handle should be an axes handle")
     end
     i=i+1;
   elseif type(varargin(i))==10
     shadingType=varargin(i);
     out=parseShading("shading",shadingType,"shading","");
     i=i+1;
   else
     _error('shading : argument of unexpected type')
   end
end

win=get('current_figure');
IMD=win.immediate_drawing;
win.immediate_drawing="off";

h=get(ax,'children');

for i=1:size(h,1);
  if h(i).type=="Fac3d" 
    if shadingType=="interp"& size(h(i).data.x)==size(h(i).data.color)
      h(i).color_flag=3;
      h(i).color_mode=-1;
    elseif shadingType=="faceted"
     h(i).color_flag=4;
     h(i).color_mode=2;
    elseif shadingType=="flat"
     h(i).color_flag=4;
     h(i).color_mode=-1;    
    end
    for j=1:2:length(out);
      // h(i).user_data(out(j))=out(j+1); does no work with Scilabgtk 4.2
     user_data=h(i).user_data;
     user_data(out(j))=out(j+1);
     h(i).user_data=user_data;
    end
  end
end

win.immediate_drawing=IMD;

endfunction // end of _shading
