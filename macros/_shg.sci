function _shg(varargin)

win=get('current_figure');
if length(varargin)==0
  show_window(win);
else
  show_window(varargin(1));
end
endfunction
