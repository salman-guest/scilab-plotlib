function varargout=_figure(varargin) 
  
global defaultFigureUserData SCI5

_start=1;
if length(varargin)
   if typeof(varargin(_start))=="Figure"
     varargout(1)=mlist(['pltlibH','handle'],scf(h));
     return;
   elseif typeof(varargin(_start))=="constant"
     winnum=varargin(_start);	 
     varargout(1)=mlist(['pltlibH','handle'],scf(winnum));
     return;
    elseif typeof(varargin(_start))<>"string"
      _error("figure : invalid argument type")
   end
end

if winsid()==[]
  winnum=0;
else
  winnum=max(winsid())+1;
end 

win=scf(winnum);
seteventhandler('plotlib_handler');
if SCI5
  win.event_handler_enable="on";
end

win=mlist(['pltlibH','handle'],win);

//[_figure_ppty_value_list,_next]=_parse_figure_ppty_value("figure",varargin,_start);
//_update_figure(win,_figure_ppty_value_list);

for i=_start:2:length(varargin)-1
  %pltlibH_set(win,varargin(i),varargin(i+1));  
end

varargout(1)=win;

endfunction
