function setMarkAndLineProperties(e,h);

markersTable=tlist(['markers';
'.'
'+'
'x'
'f'
'd'
'^'
'v'
'o'
'*'
's'
'>'
'<'],0,1,2,4,5,6,7,9,10,11,12,13)

lineStyleTable=tlist(['lineStyles';
'-'
'--'
'-.'
'.-'
':'],1,2,5,5,4);

ax=e;

while ax.type~='Axes'
   ax=ax.parent;
end

win=ax.parent;

e_mark=e;
e_line=e;
if or(h.typeOfPlot==['quiver' 'quiver3'])
  e_mark=e.children(2:3);
  e_line=e.children([1 3]);  
end

if h.Color~='default'
  if or(h.typeOfPlot==['quiver' 'quiver3'])
    e.children(1).segs_color=0*e.children(1).segs_color+findColorIndex(win,h.Color);
    e.children(3).foreground=findColorIndex(win,h.Color);
  else
    e.foreground=findColorIndex(win,h.Color);
  end    
end

if h.Marker~='none'
  set(e_mark,'mark_mode','on');
  set(e_mark,'mark_style',markersTable(h.Marker));
  set(e_mark,'mark_size_unit','point');
  set(e_mark,'mark_size',h.MarkerSize);
  _mec=h.MarkerEdgeColor;
  _mfc=h.MarkerFaceColor;
  if _mec=='default'
    _mec=ax.user_data.ColorOrder(:,1);
  end  
  if _mfc=='none'
     set(e_mark,'mark_background',0);
  else
    set(e_mark,'mark_background',findColorIndex(win,_mfc));
  end
  set(e_mark,'mark_foreground',findColorIndex(win,_mec));
else
  set(e_mark,'mark_mode','off');
end

if h.LineStyle=='none'
  set(e_line,'line_mode','off');
elseif h.LineStyle~='default'
  set(e_line,'line_style',lineStyleTable(h.LineStyle));
end 
set(e_line,'thickness',h.LineWidth);


endfunction
