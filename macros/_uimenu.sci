function varargout=_uimenu(varargin)

parent=get('current_figure');

_start=1;
if length(varargin)
   if typeof(varargin(1))=="pltlibH"
     if varargin(1).handle.type=="uimenu" & length(varargin)==1
         scilab_uimenu(varargin(1).handle);
         varargout(1)=varargin(1);
         return;
     elseif or(varargin(1).handle.type==['Figure';'uimenu'])
       parent=varargin(1).handle;
       _start=2;
     else
        _error("uimenu : handle should be an uimenu or figure handle")
     end
   end     
end

for i=_start:2:length(varargin)-1
  if convstr(varargin(i))=='parent'
    parent=varargin(i+1);
  end
end

if typeof(parent)=='pltlibH'
  parent=parent.handle;
end  

win=parent;
while get(win,'type')<>'Figure'
  win=get(win,'parent');
end

IMD=get(win,'immediate_drawing');
set(win,'immediate_drawing','off');

h=scilab_uimenu(parent);

for i=_start:2:length(varargin)-1
  if convstr(varargin(i))<>'parent'
    set(h,varargin(i),varargin(i+1));
  end  
end

varargout(1)=mlist(['pltlibH','handle'],h);

set(win,'immediate_drawing',IMD);

endfunction
