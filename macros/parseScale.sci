function [out] = parseScale(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'XScale', 'YScale' or 'ZScale' property 
//


select type(value)
case 10 //  a string
   select value
   case 'log'
      scale='log'
   case 'linear'
      scale='linear'
   else
      _error(sprintf('%s : unknown scale type ''%s''',typeOfPlot,value));
   end
else
   _error(sprintf('%s : missing scale type mode ',typeOfPlot));
end

out=list(ppty,value);




endfunction
