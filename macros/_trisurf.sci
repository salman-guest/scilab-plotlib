function hdl=_trisurf(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0 
   load(PLOTLIB+'dinosaure.dat')
   h=gcf();
   IMD=h.immediate_drawing;
   h.immediate_drawing='off';
   hdl=_trisurf(nodes,x,y,z,rand(z));
   _axis equal
   _shading interp
   h.immediate_drawing=IMD;
else
   hdl=_mainPlot('trisurf',varargin);
end

endfunction /////
