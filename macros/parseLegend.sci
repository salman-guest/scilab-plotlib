function [mat,nbProcessed,typeOfLegend]=parseLegend(fun,parseList,start)

  mat=[];
  typeOfLegend=1;
  nbProcessed=0;
  i=start;
  while i<=length(parseList)
   if type(parseList(i))==10
        if size(parseList(i),2)>1
         mat=parseList(i);
         nbProcessed=size(mat,2);
        else
         mat=[mat;parseList(i)];
         nbProcessed=nbProcessed+1;
       end
       i=i+1;
   elseif type(parseList(i))==1
      if length(parseList(i))==1
         if  parseList(i)>=-1 & parseList(i)<=5
            typeOfLegend=parseList(i);
            i=i+1;
            nbProcessed=nbProcessed+1;
         else
             _error('legend : legend style must be a scalar between -1 and 5');
         end
       else
          _error('legend : legend style must be a scalar between -1 and 5');
       end
   end
  end

endfunction
