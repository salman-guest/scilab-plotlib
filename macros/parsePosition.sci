function out=parsePosition(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'Position' figure property 
//

select type(value)
case 1 // a matrix (must be a 4 or 6 element vector)
   if (length(value)==4) 
         out=list(ppty,value);
   else
      _error(sprintf('%s : Position vector must be a 4 element vector',typeOfPlot));
   end
else
   _error(sprintf('%s : missing Position vector ',typeOfPlot));
end




endfunction
