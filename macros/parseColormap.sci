function out=parseColormap(typeOfPlot,value,pptystring,ppty)


win=get('current_figure');
nc=size(win.user_data.Colormap,1);

select type(value)

case 10 //  a string
  if or(value==['jet','hsv','ocean','winter','rainbow','autumn','white','summer',...
    'spring','hot','gray','copper','cool','bone','red','green','blue','pink'])
        cmap=evstr(value+"colormap(nc)");
    else
       _error(sprintf('%s : %s is an unknown colormap name',typeOfPlot,value));
    end
case 1 // a matrix (must be a nx3 element vector)
   if size(value,2)==3
      cmap=value;
   else
       _error(sprintf('%s : %s spec must be a n x 3 matrix',typeOfPlot,ppty));
   end
else
   _error(sprintf('%s : missing %s spec',typeOfPlot,ppty));
end

out=list(ppty,cmap);

endfunction
