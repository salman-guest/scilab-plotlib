function hdl=_mesh(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  [u,v]=meshgrid(linspace(0,%pi,30),linspace(0,%pi,30));
  h1=_mesh(sin(v).*cos(u),sin(v).*sin(u),cos(v),'edgecolor','blue');
  _hold on
  h2=_mesh(sin(v).*cos(u),1-sin(v).*sin(u),cos(v),'edgecolor','red');
  _hold off
  _axis equal
  _axis([-1 1 -1 2 -1 1])
  hdl=[h1 h2];
else
   hdl=_mainPlot('mesh',varargin);
end

endfunction /////
