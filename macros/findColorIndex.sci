function ind=findColorIndex(win,col)

c=win.user_data.FixedColors;
ind=find(and([c(:,1)==col(1) c(:,2)==col(2) c(:,3)==col(3)],'c'));
if ind==[]
   ind=find(isnan(c(:,1)));
   if ind==[] // basic colormap is full !
      N=size(c,1);
      ind=N+1;
      win.user_data.FixedColors(N+1:2*N,:)=%nan;
      if win.user_data.Colormaptable~=[]
        win.user_data.Colormaptable=[];
        activateRGBColormap(win);
        _update_shaded_plots(win),
      end
   end
   win.color_map(ind(1),:)=col(:)';
   win.user_data.FixedColors(ind(1),:)=col(:)';
end
ind=ind(1);

endfunction
