function _C=computeColor(h)
   
   ax=h.parent;
   _win=ax.parent;
   
   Cmin=ax.user_data.CLim(1);
   Cmax=ax.user_data.CLim(2);

   activateRGBColormap(_win)
   Colormaptable=_win.user_data.Colormaptable;
   ncolors=length(Colormaptable);
   
   if h.CDataMapping=='scaled'      
     _C=round((h.CData-Cmin)/(Cmax-Cmin+%eps)*(ncolors-1))+1;
   else
     _C=h.CData;
   end  
   _C(_C<1)=1; _C(_C>ncolors)=ncolors; // treshold the colors
   _C=matrix(Colormaptable(_C),size(_C));

endfunction
