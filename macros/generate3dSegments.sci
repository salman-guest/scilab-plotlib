function [X,Y,Z]=generate3dSegments(X,Y,Z,isParametric)

//
// Generates segmets from
// numerical parametric data X,Y,Z (matrices of the same size)
//
// This function is a quick hack from SCI/macros/xdess/eval3dp.sci
// Original code due to S. Steer, (C) INRIA
//
// S. Mottelet, UTC, 2009
//

[nv,nu]=size(Z);

rev=$:-1:1;
	 
if isParametric 
//   X=X(rev,:);Y=Y(rev,:);Z=Z(rev,:); // to solve the pb with plot3d1
   X=X(:);Y=Y(:);Z=Z(:);
else
   X=X(rev);
   Z=Z(:,rev);
   X=ones(1,nu).*.matrix(X,1,nv);
   Y=matrix(Y,1,nu).*.ones(1,nv);
end

ind=ones(1,nv-1).*.[0 1 1 nv+1 nv+1 nv nv 0]+ (1:nv-1).*.[1 1 1 1 1 1 1 1];

ind2=ones(1,nu-1).*.ind+((0:nu-2)*nv).*.ones(ind);
n=prod(size(ind2));
nbVertices=2;
X=matrix(X(ind2),nbVertices,n/nbVertices);
Y=matrix(Y(ind2),nbVertices,n/nbVertices);
Z=matrix(Z(ind2),nbVertices,n/nbVertices);	 
endfunction
