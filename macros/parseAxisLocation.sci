function out=parseAxisLocation(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'axisLocation' property 
//

select ppty
case "YAxisLocation"
  if type(value)==10
     if and(value~=["left";"middle";"right"])
        _error(sprintf('%s : YAxisLocation must be equal to ""left"",""middle"" or ""right""',typeOfPlot))
    end
  else
    _error(sprintf('%s : missing YAxisLocation value',typeOfPlot));  
  end
case "XAxisLocation"
  if type(value)==10
     if and(value~=["top";"middle";"bottom";"bot"])
        _error(sprintf('%s : XAxisLocation must be equal to ""top"",""middle"" or ""bottom""',typeOfPlot))
    end
  else
    _error(sprintf('%s : missing XAxisLocation value',typeOfPlot));  
  end
case "ZAxisLocation"
  if type(value)==10
     if and(value~=["left";"middle";"right"])
        _error(sprintf('%s : ZAxisLocation must be equal to ""left"",""middle"" or ""right""',typeOfPlot))
    end
  else
    _error(sprintf('%s : missing ZAxisLocation value',typeOfPlot));  
  end
end

out=list(ppty,value);

endfunction
