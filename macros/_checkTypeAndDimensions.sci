function [_out,_errorText]=_checkTypeAndDimensions(_arg,_i,_string,_type,_dims)
  
  _out=%t;
  _errorText=[];
   if and(type(_arg)~=_type)
    _out=%f;
    _typeString(13)="function";
    _typeString(1)="matrix";    
    _errorText=sprintf("%s : argument %d must be a ",_string,_i);
    for i=1:length(_type)
      if i>1 
        _errorText=_errorText+" or ";
      end
      _errorText=_errorText+_typeString(_type(i));
    end
    if argn(1)==2
      return
    end
    _error(_errorText);
   end
  
  if argn(2)==4
    return
  end
    _size=size(_arg);
    
    _check=%f;
    for i=1:size(_dims,1)
      if and(_dims(i,:)==-1)
        _result=%t;
      elseif or(_dims(i,:)==-1)
        _result=or(size(_arg)==_dims(i,:));
      else
        _result=and(size(_arg)==_dims(i,:));
      end
      _check=_check | _result;
    end
    
    if ~_check
       _out=%f;
      _errorText=sprintf("%s : argument %d must be a matrix of size ",_string,_i);
      for i=1:size(_dims,1)
        if i>1 
          _errorText=_errorText+" or ";
        end
        _errorText=_errorText+sprintf("%s x %s",strsubst(string(_dims(i,:)),'-1','n') );
      end
      if argn(1)==2
        return
      end
      _error(_errorText);
    end

endfunction

