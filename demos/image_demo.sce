h=_figure(0);
drawlater;
clf;
load(PLOTLIB+'mandrill.dat');
n=size(map,1);
_colormap([map
map*diag([1 0 0]);
map*diag([0 1 0]);
map*diag([0 0 1])]);
for i=1:4
  _subplot(2,2,i);
  drawlater
  _image(X+(i-1)*n);
  _axis equal
  _axis tight
  _drawnow
end
  
