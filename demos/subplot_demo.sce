// tests

h=_figure(0);
drawlater;
clf;
_colormap(jetcolormap(256));

cmds=['pcolor','tripcolor','surf','surfl',...
'trisurf','trisurfl','mesh','trimesh',...
'triplot','quiver','quiver3','plot3',...
'fill','fill3','loglog','plotyy'];

for i=1:16
  ax=_subplot(4,4,i);
  execstr('_'+cmds(i));
  title(ax,cmds(i));
end  

drawnow
messagebox("Double-click on an Axes to give it position [0 0 1 1]")